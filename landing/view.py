
from django.shortcuts import render


def landing(request):
    name = 'alexwestside (c)'
    return render(request, 'landing/landing.html', locals())

def home(request):
    return render(request, 'landing/home.html', locals())